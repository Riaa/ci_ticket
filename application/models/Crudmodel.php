<?php

class Crudmodel extends CI_Model{


	public function get_products_list(){

        $query = $this->db->query("select * from products");
        return $query->result();
    }

	public function insert_product(){
		$data	=	array(
			'title'	=>	$this->input->post('title'),
			'description'	=>	$this->input->post('description')
		);
		return $this->db->insert('products',$data);
	}


	public function update_product($id) {
        $data=array(
            'title' => $this->input->post('title'),
            'description'=> $this->input->post('description')
        );
        if($id==0){
            return $this->db->insert('products',$data);
        }else{
            $this->db->where('id',$id);
            return $this->db->update('products',$data);
        }
    }
}

?>